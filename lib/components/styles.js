'use strict';

import styled from "styled-components";

const MainWindow = styled.div`
    position            : absolute;
    background-color    : black;
    top                 : 0;
    bottom              : 100px;
    left                : 0;
    right               : 0;
`;

const Board = styled.div`
    width               : fit-content;
    position            : relative;
    margin              : auto;
    top                 : 50%;
    transform           : translate(0, -50%);
    border              : 2px solid white;
`;

const BoardRow = styled.div`
    height              : 30px;
    width               : 100%;
`;

const Cell = styled.div`
    display             : inline-block;
    width               : 30px;
    height              : 30px;
    border-radius       : 8px;
    &:hover {
        background-color    : rgb(170, 170, 170);
        cursor              : pointer;
    }
`;

const CellDead = styled(Cell)`
    background-color    : black;
`;

const CellAlive = styled(Cell)`
    background-color    : white;
`;

const Controls = styled.div`
    position            : absolute;
    background-color    : black;
    height              : 100px;
    bottom              : 0;
    left                : 0;
    right               : 0;
    padding-right       : 50px;
`;

const ControlButton = styled.button`
    float               : right;
    margin-top          : 35px;
    margin-left         : 20px;
    border-radius       : 5px;
    height              : 30px;
    width               : 100px;
    background-color    : white;
    color               : black;
    border              : none;
    font-weight         : bold;
    text-transform      : uppercase;
    cursor              : pointer;   
    &:hover {
        background-color    : rgb(220, 220, 220);
    }
    &:disabled {
        background-color    : rgb(150, 150, 150);
        cursor              : not-allowed;
    }
`;

module.exports = {
    Board,
    BoardRow,
    CellDead,
    CellAlive,
    Controls,
    ControlButton,
    MainWindow
};
