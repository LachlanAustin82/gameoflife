'use strict';

const React         = require("react");
const PropTypes     = require("prop-types");
const { BoardRow }  = require("./styles");
const Cell          = require("./Cell");

module.exports = ({board, rowIx}) => {
    let cells = [];

    for ( let colIx = 0; colIx < board.getColCount(); ++colIx ) {
        cells.push(
            <Cell cell  = {board.getCell(rowIx, colIx)}
                  rowIx = {rowIx}
                  colIx = {colIx}
                  key   = {colIx}/>
        );
    }

    return <BoardRow>{cells}</BoardRow>;
};

module.exports.propTypes = {
    board           : PropTypes.object.isRequired,
    rowIx           : PropTypes.number.isRequired
};
