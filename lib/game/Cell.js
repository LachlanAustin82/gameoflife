'use strict';

// A single cell in the board.
// Contains only a single property - whether it is alive or not.
class Cell {
    constructor() {
        this.alive = false;
    }

    // Returns true if the cell is alive
    isAlive() {
        return this.alive;
    }

    // Modify the 'alive' state of the cell
    setAlive(alive) {
        this.alive = alive;
    }
}

module.exports = Cell;