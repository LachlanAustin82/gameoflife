'use strict';

const Board = require("./Board");

class Game {
    constructor() {
        this.board = new Board();
    }

    reset() {
        this.board = new Board();
    }

    nextGeneration() {
        // Create a new board, because modifying the existing board will create
        // different results based on direction of iteration.
        let clone = this.board.clone();

        for ( let rowIx = 0; rowIx < this.board.getRowCount(); ++rowIx ) {
            for ( let colIx = 0; colIx < this.board.getColCount(); ++colIx ) {
                let cell = this.board.getCell(rowIx, colIx);
                let adjacentCells = this.board.getAdjacentCells(rowIx, colIx);
                let liveCount = adjacentCells.filter(cell => cell.isAlive()).length;

                let nextCell = clone.getCell(rowIx, colIx);

                if ( cell.isAlive() ) {
                    if ( liveCount < 2 ) {
                        // A cell with fewer than two live neighbours dies of under-population
                        nextCell.setAlive(false);
                    }
                    else if ( liveCount === 2 || liveCount === 3 ) {
                        // A cell with 2 or 3 live neighbours lives on to the next generation
                        nextCell.setAlive(true);
                    }
                    else {
                        // A cell with more than 3 live neighbours dies of overcrowding
                        nextCell.setAlive(false);
                    }
                }
                else {
                    if ( liveCount === 3 ) {
                        // An empty cell with exactly 3 live neighbours "comes to life"
                        nextCell.setAlive(true);
                    }
                    else {
                        // Otherwise the cell stays dead
                        nextCell.setAlive(false);
                    }
                }
            }
        }

        this.board = clone;
    }

    toggleCell(rowIx, colIx) {
        let alive = this.board.getCell(rowIx, colIx).isAlive();
        let clone = this.board.clone();
        clone.getCell(rowIx, colIx).setAlive(!alive);
        this.board = clone;
    }

    getBoard() {
        return this.board;
    }
}

module.exports = Game;