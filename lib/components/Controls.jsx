'use strict';

const React         = require("react");
const PropTypes     = require("prop-types");

const { Controls, ControlButton } = require("./styles");

module.exports = ({onReset, onGenerate, onAutoToggle, autoOn}) => (
    <Controls>
        <ControlButton onClick  = {onReset}
                       disabled = {autoOn}>Reset</ControlButton>

        <ControlButton onClick  = {onGenerate}
                       disabled = {autoOn}>Generate</ControlButton>

        <ControlButton onClick={onAutoToggle}>{autoOn ? 'Stop' : 'Auto'}</ControlButton>
    </Controls>
);

module.exports.propTypes = {
    onReset         : PropTypes.func.isRequired,
    onGenerate      : PropTypes.func.isRequired,
    onAutoToggle    : PropTypes.func.isRequired,
    autoOn          : PropTypes.bool.isRequired
};
