'use strict';

const React = require("react");
const PropTypes = require("prop-types");
const GameContext = require("./GameContext");
const { CellAlive, CellDead } = require("./styles");

module.exports = ({cell, rowIx, colIx}) => {
    let Cell = cell.isAlive() ? CellAlive : CellDead;

    return (
        <GameContext.Consumer>
            {({onCellClick}) => (
                <Cell onClick={() => onCellClick(rowIx, colIx)}/>
            )}
        </GameContext.Consumer>
    )
};

module.exports.propTypes = {
    cell        : PropTypes.object.isRequired,
    rowIx       : PropTypes.number.isRequired,
    colIx       : PropTypes.number.isRequired
};
