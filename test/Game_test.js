'use strict';

const assert = require("assert");

const Game = require("../lib/game/Game");

describe ( "Game Tests", function() {
    let game = new Game();

    beforeEach(function() {
        // Reset the game before each test
        game.reset();
    });

    function assertAlive(rowIx, colIx) {
        assert(game.getBoard().getCell(rowIx, colIx).isAlive());
    }

    function assertDead(rowIx, colIx) {
        assert(!game.getBoard().getCell(rowIx, colIx).isAlive());
    }

    describe( "Test Cell No Wrapping", function() {
        const cell = [4, 4];
        const neighbours = [
            [3, 3],
            [4, 3],
            [5, 3],
            [3, 4],
            [5, 4],
            [3, 5],
            [4, 5],
            [5, 5]
        ];

        describe ( "Test Alive Cell", function() {
            beforeEach(function() {
                game.toggleCell(...cell);
            });

            it ( "Cell has 0 live neighbours", function() {
                game.nextGeneration();
                assertDead(...cell);
            });

            it ( "Cell has 1 live neighbour", function() {
                game.toggleCell(...neighbours[0]);
                game.nextGeneration();
                assertDead(...cell);
            });

            it ( "Cell has 2 live neighbours", function() {
                game.toggleCell(...neighbours[0]);
                game.toggleCell(...neighbours[1]);
                game.nextGeneration();
                assertAlive(...cell);
            });

            it ( "Cell has 3 live neighbours", function() {
                game.toggleCell(...neighbours[0]);
                game.toggleCell(...neighbours[1]);
                game.toggleCell(...neighbours[2]);
                game.nextGeneration();
                assertAlive(...cell);
            });

            it ( "Cell has 4 live neighbours", function() {
                game.toggleCell(...neighbours[0]);
                game.toggleCell(...neighbours[1]);
                game.toggleCell(...neighbours[2]);
                game.toggleCell(...neighbours[3]);
                game.nextGeneration();
                assertDead(...cell);
            });

            it ( "Cell has 5 live neighbours", function() {
                game.toggleCell(...neighbours[0]);
                game.toggleCell(...neighbours[1]);
                game.toggleCell(...neighbours[2]);
                game.toggleCell(...neighbours[3]);
                game.toggleCell(...neighbours[4]);
                game.nextGeneration();
                assertDead(...cell);
            });

            it ( "Cell has 6 live neighbours", function() {
                game.toggleCell(...neighbours[0]);
                game.toggleCell(...neighbours[1]);
                game.toggleCell(...neighbours[2]);
                game.toggleCell(...neighbours[3]);
                game.toggleCell(...neighbours[4]);
                game.toggleCell(...neighbours[5]);
                game.nextGeneration();
                assertDead(...cell);
            });

            it ( "Cell has 7 live neighbours", function() {
                game.toggleCell(...neighbours[0]);
                game.toggleCell(...neighbours[1]);
                game.toggleCell(...neighbours[2]);
                game.toggleCell(...neighbours[3]);
                game.toggleCell(...neighbours[4]);
                game.toggleCell(...neighbours[5]);
                game.toggleCell(...neighbours[6]);
                game.nextGeneration();
                assertDead(...cell);
            });

            it ( "Cell has 8 live neighbours", function() {
                game.toggleCell(...neighbours[0]);
                game.toggleCell(...neighbours[1]);
                game.toggleCell(...neighbours[2]);
                game.toggleCell(...neighbours[3]);
                game.toggleCell(...neighbours[4]);
                game.toggleCell(...neighbours[5]);
                game.toggleCell(...neighbours[6]);
                game.toggleCell(...neighbours[7]);
                game.nextGeneration();
                assertDead(...cell);
            });
        });

        describe ( "Test Dead Cell", function() {
            it ( "Cell has 0 live neighbours", function() {
                game.nextGeneration();
                assertDead(...cell);
            });

            it ( "Cell has 1 live neighbour", function() {
                game.toggleCell(...neighbours[0]);
                game.nextGeneration();
                assertDead(...cell);
            });

            it ( "Cell has 2 live neighbours", function() {
                game.toggleCell(...neighbours[0]);
                game.toggleCell(...neighbours[1]);
                game.nextGeneration();
                assertDead(...cell);
            });

            it ( "Cell has 3 live neighbours", function() {
                game.toggleCell(...neighbours[0]);
                game.toggleCell(...neighbours[1]);
                game.toggleCell(...neighbours[2]);
                game.nextGeneration();
                assertAlive(...cell);
            });

            it ( "Cell has 4 live neighbours", function() {
                game.toggleCell(...neighbours[0]);
                game.toggleCell(...neighbours[1]);
                game.toggleCell(...neighbours[2]);
                game.toggleCell(...neighbours[3]);
                game.nextGeneration();
                assertDead(...cell);
            });

            it ( "Cell has 5 live neighbours", function() {
                game.toggleCell(...neighbours[0]);
                game.toggleCell(...neighbours[1]);
                game.toggleCell(...neighbours[2]);
                game.toggleCell(...neighbours[3]);
                game.toggleCell(...neighbours[4]);
                game.nextGeneration();
                assertDead(...cell);
            });

            it ( "Cell has 6 live neighbours", function() {
                game.toggleCell(...neighbours[0]);
                game.toggleCell(...neighbours[1]);
                game.toggleCell(...neighbours[2]);
                game.toggleCell(...neighbours[3]);
                game.toggleCell(...neighbours[4]);
                game.toggleCell(...neighbours[5]);
                game.nextGeneration();
                assertDead(...cell);
            });

            it ( "Cell has 7 live neighbours", function() {
                game.toggleCell(...neighbours[0]);
                game.toggleCell(...neighbours[1]);
                game.toggleCell(...neighbours[2]);
                game.toggleCell(...neighbours[3]);
                game.toggleCell(...neighbours[4]);
                game.toggleCell(...neighbours[5]);
                game.toggleCell(...neighbours[6]);
                game.nextGeneration();
                assertDead(...cell);
            });

            it ( "Cell has 8 live neighbours", function() {
                game.toggleCell(...neighbours[0]);
                game.toggleCell(...neighbours[1]);
                game.toggleCell(...neighbours[2]);
                game.toggleCell(...neighbours[3]);
                game.toggleCell(...neighbours[4]);
                game.toggleCell(...neighbours[5]);
                game.toggleCell(...neighbours[6]);
                game.toggleCell(...neighbours[7]);
                game.nextGeneration();
                assertDead(...cell);
            });
        });
    });

    // Run all of the above tests again, with the cells in the top-left
    // and bottom-right corners. This should test wrapping in all directions

    describe ( "Test Cell Top-Left", function() {
        const cell = [0, 0];
        const neighbours = [
            [29, 39],
            [0, 39],
            [1, 39],
            [29, 0],
            [29, 1],
            [1, 0],
            [0, 1],
            [1, 1]
        ];

        describe ( "Test Alive Cell", function() {
            beforeEach(function() {
                game.toggleCell(...cell);
            });

            it ( "Cell has 0 live neighbours", function() {
                game.nextGeneration();
                assertDead(...cell);
            });

            it ( "Cell has 1 live neighbour", function() {
                game.toggleCell(...neighbours[0]);
                game.nextGeneration();
                assertDead(...cell);
            });

            it ( "Cell has 2 live neighbours", function() {
                game.toggleCell(...neighbours[0]);
                game.toggleCell(...neighbours[1]);
                game.nextGeneration();
                assertAlive(...cell);
            });

            it ( "Cell has 3 live neighbours", function() {
                game.toggleCell(...neighbours[0]);
                game.toggleCell(...neighbours[1]);
                game.toggleCell(...neighbours[2]);
                game.nextGeneration();
                assertAlive(0, 0);
            });

            it ( "Cell has 4 live neighbours", function() {
                game.toggleCell(...neighbours[0]);
                game.toggleCell(...neighbours[1]);
                game.toggleCell(...neighbours[2]);
                game.toggleCell(...neighbours[3]);
                game.nextGeneration();
                assertDead(...cell);
            });

            it ( "Cell has 5 live neighbours", function() {
                game.toggleCell(...neighbours[0]);
                game.toggleCell(...neighbours[1]);
                game.toggleCell(...neighbours[2]);
                game.toggleCell(...neighbours[3]);
                game.toggleCell(...neighbours[4]);
                game.nextGeneration();
                assertDead(...cell);
            });

            it ( "Cell has 6 live neighbours", function() {
                game.toggleCell(...neighbours[0]);
                game.toggleCell(...neighbours[1]);
                game.toggleCell(...neighbours[2]);
                game.toggleCell(...neighbours[3]);
                game.toggleCell(...neighbours[4]);
                game.toggleCell(...neighbours[5]);
                game.nextGeneration();
                assertDead(...cell);
            });

            it ( "Cell has 7 live neighbours", function() {
                game.toggleCell(...neighbours[0]);
                game.toggleCell(...neighbours[1]);
                game.toggleCell(...neighbours[2]);
                game.toggleCell(...neighbours[3]);
                game.toggleCell(...neighbours[4]);
                game.toggleCell(...neighbours[5]);
                game.toggleCell(...neighbours[6]);
                game.nextGeneration();
                assertDead(...cell);
            });

            it ( "Cell has 8 live neighbours", function() {
                game.toggleCell(...neighbours[0]);
                game.toggleCell(...neighbours[1]);
                game.toggleCell(...neighbours[2]);
                game.toggleCell(...neighbours[3]);
                game.toggleCell(...neighbours[4]);
                game.toggleCell(...neighbours[5]);
                game.toggleCell(...neighbours[6]);
                game.toggleCell(...neighbours[7]);
                game.nextGeneration();
                assertDead(...cell);
            });
        });

        describe ( "Test Dead Cell", function() {
            it ( "Cell has 0 live neighbours", function() {
                game.nextGeneration();
                assertDead(...cell);
            });

            it ( "Cell has 1 live neighbour", function() {
                game.toggleCell(...neighbours[0]);
                game.nextGeneration();
                assertDead(...cell);
            });

            it ( "Cell has 2 live neighbours", function() {
                game.toggleCell(...neighbours[0]);
                game.toggleCell(...neighbours[1]);
                game.nextGeneration();
                assertDead(...cell);
            });

            it ( "Cell has 3 live neighbours", function() {
                game.toggleCell(...neighbours[0]);
                game.toggleCell(...neighbours[1]);
                game.toggleCell(...neighbours[2]);
                game.nextGeneration();
                assertAlive(0, 0);
            });

            it ( "Cell has 4 live neighbours", function() {
                game.toggleCell(...neighbours[0]);
                game.toggleCell(...neighbours[1]);
                game.toggleCell(...neighbours[2]);
                game.toggleCell(...neighbours[3]);
                game.nextGeneration();
                assertDead(...cell);
            });

            it ( "Cell has 5 live neighbours", function() {
                game.toggleCell(...neighbours[0]);
                game.toggleCell(...neighbours[1]);
                game.toggleCell(...neighbours[2]);
                game.toggleCell(...neighbours[3]);
                game.toggleCell(...neighbours[4]);
                game.nextGeneration();
                assertDead(...cell);
            });

            it ( "Cell has 6 live neighbours", function() {
                game.toggleCell(...neighbours[0]);
                game.toggleCell(...neighbours[1]);
                game.toggleCell(...neighbours[2]);
                game.toggleCell(...neighbours[3]);
                game.toggleCell(...neighbours[4]);
                game.toggleCell(...neighbours[5]);
                game.nextGeneration();
                assertDead(...cell);
            });

            it ( "Cell has 7 live neighbours", function() {
                game.toggleCell(...neighbours[0]);
                game.toggleCell(...neighbours[1]);
                game.toggleCell(...neighbours[2]);
                game.toggleCell(...neighbours[3]);
                game.toggleCell(...neighbours[4]);
                game.toggleCell(...neighbours[5]);
                game.toggleCell(...neighbours[6]);
                game.nextGeneration();
                assertDead(...cell);
            });

            it ( "Cell has 8 live neighbours", function() {
                game.toggleCell(...neighbours[0]);
                game.toggleCell(...neighbours[1]);
                game.toggleCell(...neighbours[2]);
                game.toggleCell(...neighbours[3]);
                game.toggleCell(...neighbours[4]);
                game.toggleCell(...neighbours[5]);
                game.toggleCell(...neighbours[6]);
                game.toggleCell(...neighbours[7]);
                game.nextGeneration();
                assertDead(...cell);
            });
        });
    });

    // This will test wrapping in the down and right direction
    describe ( "Test Cell Bottom-Right", function() {
        const cell = [29, 39];
        const neighbours = [
            [0, 38],
            [0, 39],
            [0, 0],
            [28, 0],
            [29, 0],
            [28, 38],
            [29, 38],
            [28, 39]
        ];

        describe ( "Test Alive Cell", function() {
            beforeEach(function() {
                game.toggleCell(...cell);
            });

            it ( "Cell has 0 live neighbours", function() {
                game.nextGeneration();
                assertDead(...cell);
            });

            it ( "Cell has 1 live neighbour", function() {
                game.toggleCell(...neighbours[0]);
                game.nextGeneration();
                assertDead(...cell);
            });

            it ( "Cell has 2 live neighbours", function() {
                game.toggleCell(...neighbours[0]);
                game.toggleCell(...neighbours[1]);
                game.nextGeneration();
                assertAlive(...cell);
            });

            it ( "Cell has 3 live neighbours", function() {
                game.toggleCell(...neighbours[0]);
                game.toggleCell(...neighbours[1]);
                game.toggleCell(...neighbours[2]);
                game.nextGeneration();
                assertAlive(...cell);
            });

            it ( "Cell has 4 live neighbours", function() {
                game.toggleCell(...neighbours[0]);
                game.toggleCell(...neighbours[1]);
                game.toggleCell(...neighbours[2]);
                game.toggleCell(...neighbours[3]);
                game.nextGeneration();
                assertDead(...cell);
            });

            it ( "Cell has 5 live neighbours", function() {
                game.toggleCell(...neighbours[0]);
                game.toggleCell(...neighbours[1]);
                game.toggleCell(...neighbours[2]);
                game.toggleCell(...neighbours[3]);
                game.toggleCell(...neighbours[4]);
                game.nextGeneration();
                assertDead(...cell);
            });

            it ( "Cell has 6 live neighbours", function() {
                game.toggleCell(...neighbours[0]);
                game.toggleCell(...neighbours[1]);
                game.toggleCell(...neighbours[2]);
                game.toggleCell(...neighbours[3]);
                game.toggleCell(...neighbours[4]);
                game.toggleCell(...neighbours[5]);
                game.nextGeneration();
                assertDead(...cell);
            });

            it ( "Cell has 7 live neighbours", function() {
                game.toggleCell(...neighbours[0]);
                game.toggleCell(...neighbours[1]);
                game.toggleCell(...neighbours[2]);
                game.toggleCell(...neighbours[3]);
                game.toggleCell(...neighbours[4]);
                game.toggleCell(...neighbours[5]);
                game.toggleCell(...neighbours[6]);
                game.nextGeneration();
                assertDead(...cell);
            });

            it ( "Cell has 8 live neighbours", function() {
                game.toggleCell(...neighbours[0]);
                game.toggleCell(...neighbours[1]);
                game.toggleCell(...neighbours[2]);
                game.toggleCell(...neighbours[3]);
                game.toggleCell(...neighbours[4]);
                game.toggleCell(...neighbours[5]);
                game.toggleCell(...neighbours[6]);
                game.toggleCell(...neighbours[7]);
                game.nextGeneration();
                assertDead(...cell);
            });
        });

        describe ( "Test Dead Cell", function() {
            it ( "Cell has 0 live neighbours", function() {
                game.nextGeneration();
                assertDead(...cell);
            });

            it ( "Cell has 1 live neighbour", function() {
                game.toggleCell(...neighbours[0]);
                game.nextGeneration();
                assertDead(...cell);
            });

            it ( "Cell has 2 live neighbours", function() {
                game.toggleCell(...neighbours[0]);
                game.toggleCell(...neighbours[1]);
                game.nextGeneration();
                assertDead(...cell);
            });

            it ( "Cell has 3 live neighbours", function() {
                game.toggleCell(...neighbours[0]);
                game.toggleCell(...neighbours[1]);
                game.toggleCell(...neighbours[2]);
                game.nextGeneration();
                assertAlive(...cell);
            });

            it ( "Cell has 4 live neighbours", function() {
                game.toggleCell(...neighbours[0]);
                game.toggleCell(...neighbours[1]);
                game.toggleCell(...neighbours[2]);
                game.toggleCell(...neighbours[3]);
                game.nextGeneration();
                assertDead(...cell);
            });

            it ( "Cell has 5 live neighbours", function() {
                game.toggleCell(...neighbours[0]);
                game.toggleCell(...neighbours[1]);
                game.toggleCell(...neighbours[2]);
                game.toggleCell(...neighbours[3]);
                game.toggleCell(...neighbours[4]);
                game.nextGeneration();
                assertDead(...cell);
            });

            it ( "Cell has 6 live neighbours", function() {
                game.toggleCell(...neighbours[0]);
                game.toggleCell(...neighbours[1]);
                game.toggleCell(...neighbours[2]);
                game.toggleCell(...neighbours[3]);
                game.toggleCell(...neighbours[4]);
                game.toggleCell(...neighbours[5]);
                game.nextGeneration();
                assertDead(...cell);
            });

            it ( "Cell has 7 live neighbours", function() {
                game.toggleCell(...neighbours[0]);
                game.toggleCell(...neighbours[1]);
                game.toggleCell(...neighbours[2]);
                game.toggleCell(...neighbours[3]);
                game.toggleCell(...neighbours[4]);
                game.toggleCell(...neighbours[5]);
                game.toggleCell(...neighbours[6]);
                game.nextGeneration();
                assertDead(...cell);
            });

            it ( "Cell has 8 live neighbours", function() {
                game.toggleCell(...neighbours[0]);
                game.toggleCell(...neighbours[1]);
                game.toggleCell(...neighbours[2]);
                game.toggleCell(...neighbours[3]);
                game.toggleCell(...neighbours[4]);
                game.toggleCell(...neighbours[5]);
                game.toggleCell(...neighbours[6]);
                game.toggleCell(...neighbours[7]);
                game.nextGeneration();
                assertDead(...cell);
            });
        });
    });
});