'use strict';

const React     = require("react");
const PropTypes = require("prop-types");
const { Board } = require("./styles");
const BoardRow  = require("./BoardRow");

module.exports = ({board}) => {
    let rows = [];

    for ( let rowIx = 0; rowIx < board.getRowCount(); ++rowIx ) {
        rows.push(
            <BoardRow board = {board}
                      rowIx = {rowIx}
                      key   = {rowIx}/>
        );
    }

    return <Board>{rows}</Board>;
};

module.exports.propTypes = {
    board : PropTypes.object.isRequired
};
