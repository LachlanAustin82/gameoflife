'use strict';

const gulp              = require("gulp");
const browserify        = require("browserify");
const babelify          = require("babelify");
const source            = require("vinyl-source-stream");
const buffer            = require("vinyl-buffer");
const sourcemaps        = require("gulp-sourcemaps");
const presetEnv         = require("@babel/preset-env");
const react             = require("@babel/preset-react");
const transformRuntime = require("@babel/plugin-transform-runtime");

gulp.task("dev", function() {
    browserify({
        basedir         : '.',
        debug           : true,
        entries         : [ "./lib/client.jsx" ],
        paths           : [ './' ],
        extensions      : [ '.jsx', '.js' ],
        cache           : {},
        packageCache    : {}
    })
    .transform(babelify, {
        global      : true,
        presets     : [ react, presetEnv ],
        plugins     : [
            [ transformRuntime, { helpers: false, regenerator: true } ]
        ]
    })
    .bundle()
    .on('error', (err) => {
        console.log("Failed with error: " + err);
        process.exit(1);
    })
    .pipe(source('./generated/client.js'))
    .pipe(buffer())
    .pipe(sourcemaps.init({loadMaps: true}))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./public'));
});
