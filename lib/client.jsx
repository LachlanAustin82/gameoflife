'use strict';

const React = require("react");
const ReactDOM = require("react-dom");

const Game = require("./game/Game");
const Main = require("./components/Main");


ReactDOM.render(<Main game={new Game()}/>, document.getElementById('react-main-mount'));
