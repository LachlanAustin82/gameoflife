'use strict';

const React             = require("react");
const PropTypes         = require("prop-types");

const { MainWindow }    = require("./styles");
const Controls          = require("./Controls");
const Board             = require("./Board");
const GameContext       = require("./GameContext");

class Main extends React.Component {
    constructor(props) {
        super(props);

        this.state = { auto: null };
    }

    componentWillUnmount() {
        if ( this.state.auto ) {
            // If the auto mode is on when component unmounts, ensure to turn it off!!
            clearInterval(this.state.auto);
        }
    }

    render() {
        // Create GameContext, so we don't have to pass the cell click handler
        // through the entire component hierarchy
        let gameContext = {onCellClick: this.__handleCellClick.bind(this)};

        return (
            <GameContext.Provider value={gameContext}>
                <MainWindow>
                    <Board board={this.props.game.getBoard()}/>
                </MainWindow>

                <Controls
                    onReset         = {this.__handleReset.bind(this)}
                    onGenerate      = {this.__handleGenerate.bind(this)}
                    onAutoToggle    = {this.__handleAutoToggle.bind(this)}
                    autoOn          = {!!this.state.auto}/>
            </GameContext.Provider>
        )
    }

    // Generate the next interval of the simulation
    __handleGenerate() {
        this.props.game.nextGeneration();
        this.forceUpdate();
    }

    // Reset the simulation
    __handleReset() {
        this.props.game.reset();
        this.forceUpdate();
    }

    // Handle a cell being clicked. This will toggle the value in the cell
    __handleCellClick(rowIx, colIx) {
        this.props.game.toggleCell(rowIx, colIx);
        this.forceUpdate();
    }

    // Toggle auto mode on/off
    __handleAutoToggle() {
        if ( this.state.auto ) {
            clearInterval(this.state.auto);
            this.setState({auto: null});
        }
        else {
            let auto = setInterval(() => {
                this.__handleGenerate();
            }, 100);

            this.setState({auto});
        }
    }
}

Main.propTypes = {
    game        : PropTypes.object.isRequired
};

module.exports = Main;
