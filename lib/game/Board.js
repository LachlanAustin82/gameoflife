'use strict';

const Cell = require("./Cell");

const ROW_COUNT = 30;
const COL_COUNT = 40;

// Class to represent a board state.
// Board state can be cloned so that it can be modified without mutating the original.
class Board {
    constructor() {
        this.rowCount = ROW_COUNT;
        this.colCount = COL_COUNT;
        this.cells = [];

        for ( let rowIx = 0; rowIx < this.rowCount; ++rowIx ) {
            let row = [];

            for ( let colIx = 0; colIx < this.colCount; ++colIx ) {
                row.push(new Cell(rowIx, colIx));
            }

            this.cells.push(row);
        }
    }

    // Clone the existing board
    // @returns Board
    clone() {
        let clone = new Board();

        for ( let rowIx = 0; rowIx < this.rowCount; ++rowIx ) {
            for ( let colIx = 0; colIx < this.colCount; ++colIx ) {
                if ( this.getCell(rowIx, colIx).isAlive() ) {
                    clone.getCell(rowIx, colIx).setAlive(true);
                }
            }
        }

        return clone;
    }

    // Get an individual cell at (rowIx, colIx)
    getCell(rowIx, colIx) {
        return this.cells[rowIx][colIx];
    }

    // Get all adjacent cells to the cell at (rowIx, colIx)
    getAdjacentCells(rowIx, colIx) {
        // It is not specified whether the board wraps around when considering
        // neighbours in all cases. However, it is the case for 'coming back to life',
        // so we will assume it is true for all cases.

        let coords = [
            this.__getWrappedCoord(rowIx - 1, colIx - 1),
            this.__getWrappedCoord(rowIx - 1, colIx),
            this.__getWrappedCoord(rowIx - 1, colIx + 1),
            this.__getWrappedCoord(rowIx, colIx - 1),
            this.__getWrappedCoord(rowIx, colIx + 1),
            this.__getWrappedCoord(rowIx + 1, colIx - 1),
            this.__getWrappedCoord(rowIx + 1, colIx),
            this.__getWrappedCoord(rowIx + 1, colIx + 1)
        ];

        return coords.map(({rowIx, colIx}) => {
            return this.getCell(rowIx, colIx);
        });
    }

    // Get the number of rows on the board
    getRowCount() {
        return this.rowCount;
    }

    // Get the number of cols on the board
    getColCount() {
        return this.colCount;
    }

    // Adjust a coordinate as if it had wrapped around the board
    __getWrappedCoord(rowIx, colIx) {
        // This will put the number into the range (-ROW_COUNT, ROW_COUNT)
        rowIx = rowIx % this.rowCount;
        colIx = colIx % this.colCount;

        // If negative, add ROW/COL COUNT to bring into range [0, ROW_COUNT)
        if ( rowIx < 0 ) {
            rowIx += this.rowCount;
        }
        if ( colIx < 0 ) {
            colIx += this.colCount;
        }

        return { rowIx, colIx };
    }
}

module.exports = Board;